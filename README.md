Application Server with dynamic JAR loader
=====

### Description
Dynamic JAR (application) loader example.
Basic principles:
 1. Each application implements the same [mx.mtm.poc.appserver.common.interfaces.ExecutableApplication](https://gitlab.com/mx.mtm.poc/apploader/blob/master/common/src/main/java/mx/mtm/poc/appserver/common/interfaces/ExecutableApplication.java) interface
 2. Each application uses the same file structure - main class is <pre>mx.mtm.poc.appserver.apps.Application</pre>
 3. Main server application tries to load all the jars given via command line params
  
### Running
<pre>java -jar target/loader-jar-with-dependencies.jar JAR_FILE_1 JAR_FILE_2 JAR_FILE_3 ...</pre>

