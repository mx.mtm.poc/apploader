package mx.mtm.poc.appserver.apps;

import mx.mtm.poc.appserver.common.interfaces.ExecutableApplication;

/**
 * @author qtoosha
 * @since 2016.07.31
 */
public class Application implements ExecutableApplication {

    @Override
    public void execute() {
        System.out.println("I am BAR");
    }

    /**
     * Main entry point
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        new Application().execute();
    }
}
