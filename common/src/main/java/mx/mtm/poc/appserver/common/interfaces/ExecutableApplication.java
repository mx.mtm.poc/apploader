package mx.mtm.poc.appserver.common.interfaces;

/**
 * @author qtoosha
 * @since 2016.07.31
 */
public interface ExecutableApplication {

    /**
     * Main exec method
     */
    void execute();
}
