package mx.mtm.poc.appserver.loader;


import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import mx.mtm.poc.appserver.common.interfaces.ExecutableApplication;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author qtoosha
 * @since 2016.07.31
 */
@Getter
@Setter
@RequiredArgsConstructor
public class ApplicationLoader implements ExecutableApplication {

    @NonNull
    private String jar;

    @SuppressWarnings("unchecked")
    @Override
    public void execute() {
        System.out.println("Loading file: " + this.jar);


        try {
            File file = new File(this.getJar());
            if (!file.canRead()) {
                throw new RuntimeException("File is not readable: " + this.getJar());
            }

            URL[] urls = new URL[]{new URL("file://" + file.getAbsolutePath())};
            System.out.println("JAR URL: " + urls[0].toString());

            URLClassLoader classLoader = new URLClassLoader(urls, this.getClass().getClassLoader());
            Class<ExecutableApplication> clazz = (Class<ExecutableApplication>) Class.forName("mx.mtm.poc.appserver.apps.Application", true, classLoader);

            System.out.println("Loading class: " + clazz.getCanonicalName());

            clazz.newInstance().execute();
        } catch (MalformedURLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Main entry point
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            throw new RuntimeException("Please specify JAR file to load");
        }

        for (String arg : args) {
            new ApplicationLoader(arg).execute();
        }
    }
}
